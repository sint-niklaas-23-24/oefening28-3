﻿namespace Oefening28_3
{
    class Docent
    {
        private string _naam;
        private List<Vak> _vakken = new List<Vak>();

        public Docent(string naam)
        {
            Naam = naam;

        }
        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }
        public List<Vak> Vakken
        {
            get { return _vakken; }
            set { _vakken = value; }
        }
        public void AddVak(Vak vak)
        {
            Vakken.Add(vak);
        }
        public void RemoveVak(Vak vak)
        {
            Vakken.Remove(vak);
        }
        public override string ToString()
        {
            string resultaat;
            resultaat = Naam + " geeft de volgende vakken: " + Environment.NewLine;
            foreach (Vak vak in Vakken)
            {
                resultaat += vak.ToString() + Environment.NewLine;
            }
            return resultaat;
        }
    }
}
