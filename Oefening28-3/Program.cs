﻿namespace Oefening28_3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Docent d = new Docent("hvs");
            Vak v1 = new Vak("Basic c#", 11, "Lok1");
            Vak v2 = new Vak("Advanced c#", 13, "Lok1");
            Vak v3 = new Vak("Web", 12, "Lok7");
            Vak v4 = new Vak("Git", 2, "Lok9");

            d.AddVak(v1);
            d.AddVak(v2);
            d.AddVak(v3);
            d.AddVak(v4);

            Console.WriteLine(d.ToString());
            d.RemoveVak(v3);
            Console.WriteLine("na het verwijderen van Web");
            Console.WriteLine(d.ToString());
        }
    }
}
