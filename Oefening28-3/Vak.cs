﻿namespace Oefening28_3
{
    class Vak
    {
        private string _beschrijving;
        private string _leslokaal;
        private int _lesUren;

        public Vak(string beschrijving, int lesuren, string leslokaal)
        {
            Beschrijving = beschrijving;
            LesUren = lesuren;
            Leslokaal = leslokaal;
        }
        public string Beschrijving
        {
            get { return _beschrijving; }
            set { _beschrijving = value; }
        }
        public string Leslokaal
        {
            get { return _leslokaal; }
            set { _leslokaal = value; }
        }
        public int LesUren
        {
            get { return _lesUren; }
            set { _lesUren = value; }
        }
        public bool Equals(object obj)
        {
            bool resultaat = false;
            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Vak v = (Vak)obj;
                    if (this.Beschrijving == v.Beschrijving && this.Leslokaal == v.Leslokaal && this.LesUren == v.LesUren)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
        public override string ToString()
        {
            return $"{Beschrijving} - {LesUren} - {Leslokaal}";
        }

    }
}
